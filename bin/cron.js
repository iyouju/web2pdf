/*
 * @Descripttion: 
 * @version: v1.0
 * @Author: 薛玉峰
 * @Date: 2020-07-01 17:52:47
 * @LastEditors: 薛玉峰
 * @LastEditTime: 2020-07-03 09:41:16
 */
const puppeteer = require('puppeteer');
const moment = require('moment');
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
// const proxyChain = require('proxy-chain');

// const oldProxyUrl = 'http://ip.yqie.com/ipproxy.htm';

    
// 递归创建目录 同步方法
function mkdirsSync(dirname) {
    if (fs.existsSync(dirname)) {
      return true;
    } else {
      if (mkdirsSync(path.dirname(dirname))) {
        fs.mkdirSync(dirname);
        return true;
      }
    }
}

// 生产格式化，关键词
class web2pdf {
    
    saveFilePath = './public/pdf/';
    pageSize = 10;  // 每页多少条
    limit = 3;   // 采集翻页3页
    
    async pageUp(data) {
        console.log('===《 %s 》====', data.wd)
        for (let i = 1; i <= this.limit; i++) {
            await this.pdf(i, data)
        }
    }

    async pdf(p, data) {

        try {

            console.log('第%s页', p);
            p = p || 1;
            
            let wd = encodeURIComponent(data.wd);
            let offset = (p - 1) * this.pageSize;
            let url = 'https://www.baidu.com/s?wd="'+wd+'"&pn='+offset+'&oq="'+wd+'"';
            // const newProxyUrl = await proxyChain.anonymizeProxy(oldProxyUrl);
            const browser = await puppeteer.launch({ 
                headless: true
                // headless: false,
                // args: [
                //     '--no-sandbox',
                //     '--disable-setuid-sandbox',
                //     `--proxy-server=${newProxyUrl}`
                // ]
            })
            const page = await browser.newPage();
            //选择要打开的网页 
            await page.goto(url, { waitUntil: 'networkidle0'})

            let day = moment().format("YYYY-MM-DD");
            let h = moment().format("HH");
            //选择你要输出的那个PDF文件路径，把爬取到的内容输出到PDF中，必须是存在的PDF，可以是空内容，如果不是空的内容PDF，那么会覆盖内容
            let pdfFilePath = this.saveFilePath+data.key+'/'+day+'/'+h+'/';
            //根据你的配置选项，我们这里选择A4纸的规格输出PDF，方便打印
            mkdirsSync(pdfFilePath);
            
            await page.screenshot({
                path: pdfFilePath+p+'.jpg',
                scale: 1,
                width: '8.27in',
                height: '16.7in',
                printBackground: true,
                landscape: false,
                displayHeaderFooter: false
            });
            
            await page.pdf({
                path: pdfFilePath+p+'.pdf',
                scale: 1,
                width: '8.27in',
                height: '16.7in',
                printBackground: true,
                landscape: false,
                displayHeaderFooter: false
            });
            console.log('生成PDF: %s', pdfFilePath+p+'.pdf');

            await browser.close();
            let _time = (Math.ceil(Math.random()*10)+5)*1000;
            await setTimeout(x => {}, _time);

        } catch (e) {
            console.error(e)  
        }
    }

    async writeFileWord() {
        
        console.log('>Start 初始化词库');

        let that = this;
        let txt = fs.readFileSync('words.txt');
        txt = txt.toString();
        let snsArr = txt.split(/[(\r\n)\r\n]+/);
        snsArr.forEach((item, index) => {
            if (!item) {
                snsArr.splice(index, 1);//删除空项
            } else {
                console.log(index, '"'+item+'"')
            }
        });

        // let str = JSON.stringify(snsArr, null, "\t");
        let _obj = [];
        for (let i in snsArr) {
            // let md5str = crypto.createHash('md5').update(snsArr[i]).digest("hex");
            _obj[i] = {
                'key' : crypto.createHash('md5').update(snsArr[i]).digest("hex"),
                'wd' : snsArr[i]
            };
        }
        let str = JSON.stringify(_obj);
        fs.writeFileSync('data.txt', str);

        console.log('>END 初始化词库<');
        
        console.log('>Start 处理网页转换为PDF 每个关键词采集%s页', that.limit);

        
        for (let v of _obj) {
            
            await that.pageUp(v);
            let _time = (Math.ceil(Math.random()*10)+10)*1000;
            await setTimeout(x => {}, _time);
        }
        console.timeEnd('时间统计');
        console.log('>END');
    }
    
    /* writeFileWord() {
        
        let that = this;
        fs.readFile('words.txt', function(error, txt){    //读取文件，回调函数第一个参数表示错误信息，第二个参数为读取的文本内容
            
            if (error) {
                console.log(error);
            } else {
                console.log('>Start 初始化词库');
    
                txt = txt.toString();
                let snsArr = txt.split(/[(\r\n)\r\n]+/);
                snsArr.forEach((item, index) => {
                    if (!item) {
                        snsArr.splice(index, 1);//删除空项
                    } else {
                        console.log(index, '"'+item+'"')
                    }
                });
    
                // let str = JSON.stringify(snsArr, null, "\t");
                let _obj = [];
                for (let i in snsArr) {
                    // let md5str = crypto.createHash('md5').update(snsArr[i]).digest("hex");
                    _obj[i] = {
                        'key' : crypto.createHash('md5').update(snsArr[i]).digest("hex"),
                        'wd' : snsArr[i]
                    };
                }
                let str = JSON.stringify(_obj);
                fs.writeFileSync('data.txt', str);
                console.log('>END 初始化词库');
                console.log('>Start 处理网页转换为PDF 每个关键词采集%s页', that.limit);
    
                for (let v of _obj) {
                    
                    setTimeout(x => {
                        that.pageUp(v);
                    }, 5000);
                }
                
                // console.log('>END 处理网页转换为PDF');
                // console.log(snsArr)
            }
        });
    } */

}

let pdf = new web2pdf();
console.time('时间统计'); 

pdf.writeFileWord();


