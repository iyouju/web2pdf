/*
 * @Descripttion: 橙牛洗车路由模块
 * @version: v1.0
 * @Author: 薛玉峰
 * @Date: 2020-04-23 16:27:38
 * @LastEditors: 薛玉峰
 * @LastEditTime: 2020-07-03 09:40:49
 */
var express = require('express');
var router = express.Router();

var path = require('path');

require('../bin/cron');

var appPath = path.join(path.dirname(__dirname), 'public');
var static = express.static(path.join(appPath,'index.html'));

router.use('/', require('./index'));
router.use('/so', require('./so'));


module.exports = router;
