let jwt = require('jsonwebtoken');
let request = require('request');
// 生成token
let jwtTokenSecret = 'W$H^&%JI*)_+(BFGNgf5h6'; //加密token 校验token时要使用
let token = 'Bearer '+jwt.sign({
        "PhoneNumbers": "15501284059",  // 接收者手机号
        "TemplateCode": "SMS_193522864",  // 模板编号
        "TemplateParam": JSON.stringify({code: 8888}) // 模板对应的参数
    }, jwtTokenSecret, {
        expiresIn: 60 * 1 //  过期时间
    });
console.log("\n 生成token:\n===============================\n %s \n===============================", token);

let param = jwt.decode(token.split(' ')[1]);
console.log("解密token:\n\n %s \n", param);


// 接口调用demo

let jwt = require('jsonwebtoken');
let request = require('request');

let jwtTokenSecret = 'W$H^&%JI*)_+(BFGNgf5h6'; // 密钥 校验token时要使用
// 生成token
let token = 'Bearer '+jwt.sign({
        "PhoneNumbers": "15501284059",  // 接收者手机号
        "TemplateCode": "SMS_193522864",  // 模板编号
        "TemplateParam": JSON.stringify({code: 8888}) // 模板对应的参数
    }, jwtTokenSecret, {
        expiresIn: 60 * 1 //  过期时间
    });

let options = {
    url: 'http://127.0.0.1:8015/sms/send',
    headers: {
        'User-Agent': 'request',
        'authorization': token
    }
}
function callback(error, response, body) {
    console.log(body);
}

// 发送注册验证码   输出：【佰付美】验证码是8888，您使用的是佰付美的注册功能，请于5分钟内正确输入
request(options, callback);

options.headers.authorization = 'Bearer '+jwt.sign({
    "PhoneNumbers": "15501284059",  // 接收者手机号
    "TemplateCode": "SMS_193522862",  // 模板编号
    "TemplateParam": JSON.stringify({code: 8888}) // 模板对应的参数
}, jwtTokenSecret, {
    expiresIn: 60 * 1 //  过期时间
});

// 发送登录验证码 输出：【佰付美】验证码是8888，您使用的是佰付美的登录功能，请于5分钟内正确输入
request(options, callback);





