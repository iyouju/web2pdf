/*
 * @Descripttion:  短信接口
 * @version: v1.0
 * @Author: 薛玉峰
 * @Date: 2020-04-23 18:02:50
 * @LastEditors: 薛玉峰
 * @LastEditTime: 2020-06-23 14:50:37
 */

class SmsInterface {
    constructor() {}
    client() {}
    send() {}
}

module.exports = SmsInterface;