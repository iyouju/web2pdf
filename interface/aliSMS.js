/*
 * @Descripttion:  阿里云短信接口
 * @version: v1.0
 * @Author: 薛玉峰
 * @Date: 2020-04-23 18:02:50
 * @LastEditors: 薛玉峰
 * @LastEditTime: 2020-07-01 14:13:44
 */
const SmsInterface = require('./SmsInterface');
const Core = require('@alicloud/pop-core');

// 阿里云申请
const accessKeyId = '';   
const accessSecret = '';

class aliSMS extends SmsInterface {

    clientHttp = null;
    constructor() {
        super();
    }
    
    client () {

        if (!this.clientHttp) {
            this.clientHttp = new Core({
                accessKeyId: accessKeyId,
                accessKeySecret: accessSecret,
                endpoint: 'https://dysmsapi.aliyuncs.com',
                apiVersion: '2017-05-25'
            });
        }
        
        return this.clientHttp;
    }
    
    /**
     * 短信发送接口
     * 
     * @param Object params{
     *      RegionId        string "cn-hangzhou" 目前写死  
     *      PhoneNumbers    string 接收短信的手机号码。格式：国内短信：11位手机号码，例如15951955195。国际/港澳台消息：国际区号+号码，例如85200000000。支持对多个手机号码发送短信，手机号码之间以英文逗号（,）分隔。上限为1000个手机号码。
     *      SignName        string 短信签名名称。请在控制台签名管理页面签名名称一列查看。
     *      TemplateCode    string 短信模板ID。请在控制台模板管理页面模板CODE一列查看。
     *      TemplateParam   string 短信模板变量对应的实际值，JSON格式。
     * }
     * @return: function callback({
     *      BizId	    String	900619746936498440^0  发送回执ID，可根据该ID在接口QuerySendDetails中查询具体的发送状态。
     *      Code	    String	OK	请求状态码。返回OK代表请求成功。其他错误码详见阿里云短信错误码列表。
     *      Message	    String	OK	状态码的描述。
     *      RequestId	String	F655A8D5-B967-440B-8683-DAD6FF8DE990 请求ID。
     * })
     */
    send (params, callback) {
        
        // SMS_191095097 "验证码${code}，您正在进行身份验证，打死不要告诉别人哦！"
        this.client();
        let requestOption = {
            method: 'POST'
        };
        
        this.clientHttp.request('SendSms', params, requestOption).then((result) => {
            callback({status: 200, message: '发送成功', response: result});
        }, (ex) => {
            console.error(ex);
            callback({status: 404, message: 'Error!', response: ex});
        });
    }

    /**
     * 批量发送
     * 
     * 接口文档：https://api.aliyun.com/?spm=a2c4g.11186623.2.21.4e5d50a4Bvi0tO#/?product=Dysmsapi&version=2017-05-25&api=SendBatchSms&tab=DOC&lang=NODEJS
     * @param Object params{
     *      RegionId        string "cn-hangzhou" 目前写死
     *      PhoneNumberJson string ["15900000000","13500000000"] 接收短信的手机号码，JSON数组格式。
     *      SignNameJson string ["阿里云","阿里巴巴"] 短信签名名称，JSON数组格式。
     *      TemplateCode string 短信模板CODE。请在控制台模板管理页面模板CODE一列查看。
     *      TemplateParamJson string 短信模板变量对应的实际值，JSON格式。
     * }
     * 
     * @return: function callback({
     *      BizId	    String	900619746936498440^0  发送回执ID，可根据该ID在接口QuerySendDetails中查询具体的发送状态。
     *      Code	    String	OK	请求状态码。返回OK代表请求成功。其他错误码详见阿里云短信错误码列表。
     *      Message	    String	OK	状态码的描述。
     *      RequestId	String	F655A8D5-B967-440B-8683-DAD6FF8DE990 请求ID。
     * })
     */
    sendBatch (params, callback) {
        
        this.client();
        let requestOption = {
            method: 'POST'
        };
        
        this.clientHttp.request('SendBatchSms', params, requestOption).then((result) => {
            // console.log(JSON.stringify(result));
            callback(result);
        }, (ex) => {
            console.error(ex);
            callback(ex);
        });
    }
}

module.exports = aliSMS;